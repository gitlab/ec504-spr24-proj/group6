# Alex
- Accepted Feedback:
    - Removed stack traces from error messages for the scrawler
    - Added displaying number of pages crawled thus far regardless of --stats argument
    - Blacklisted assets.tumblr.com
    - Added check for seed URLs not starting with "http"
    - Added to INSTALL.md:
        - Added some common websites to try crawling
        - Added some instructions for seed URLs
- Rejected Feedback:
    - n/a
- Other Changes:
    - Emphasized the reduced amount of URLs available when running without --xl
    - Crawler now fetches a new URL without incrementing the crawl count if the URL contains a file extension for a image, video, audio, or PDF


# Reza
- Accepted Feedbacks:
    - Updated the usuage, workflow, limitations etc. in ReadMe.md and Install.md based on the given feadbacks and clarified the confusions.
    - Changed the color range of Checker GUI and add an spectrum of colors using to show the confidence score in a legend.
    - Fixed bugs resulted in case of using a special charachter for both checker and corrector.
    - Increased the speed of corrector's GUI and fix reported bug for running out of heap memory.
    - Fixed corrector bugs of not updating the typos in the final correction.
    - Increased accuracy of checker by adding the n-grams in everywhere. Previously for some methods it was missed such as in the GUI. Databases has been updated as well with new crawled data.
    - Increased accuracy of corrector by introducing new method (we called it similarity) on top of the previous mehtods. Databases has been updated as well with new crawled data.
- Other Changes:
    - Turkish language option is added for checker and corrector. It is a new feature which is asked in the project description.
- Rejected FeedBacks:
    - Improve the accruacy of translation. This is because of the fact that translation can be futher improved using the corrector itself. Adding it as an automatic tool would add redundency to the tool.
    - State Machine View. This feature was not implemented due to the fact that it is not effective in the performance or user-friendliness of the tool. Also, due to the limited time we spent more time on more important parts.
    - Putting Crawler and Checker/Corrector modules in the same directory. Doing this would result in unnecessary conflicts in git and since these two elements are not a part of each other and every crawled data can be address through the other directory we didn't implement it.

# Michael
- Accepted Feedback:
    - Added to INSTALL.md:
        - Added instructions to run Android App via Android Studio
- Rejected Feedback:
    - n/a
- Other Changes:
    - Added external library file for the Regex Parser,as it was not compiling in the MakeFile
    - Added a File Reader library to README, as that was missing from the external libraries section.
    - Updated logic of Android App Checker to the latest version (in CheckerCorrector Folder) so that it represents a more up-to-date version of the implemented checker.
    - Fixed SQL library import in Android App (Binary of library is provided) as the SQL package used by the checker wasn't able to import successfully from a .jar. Putting the binary file of the SQL library in the app resolved this issue.
    - Provided Zipped files of Android Studio Enviroment and modified Checker package so that users can compile their own version of the app (with edits) in addition to seeing the modified version of the checker for Android app compatibility.
