import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;

import GUI.*;
import HashTableMaker.HashTableMaker;
import DirectedGraph.BasicGraph;
import DirectedGraph.DirectedGraph;
import HashTableMaker.HashTableMaker;
import StateMachine.*;
import DBinterface.DBinterface;
import util.*;

public class Checker {
     public static void main(String[] args) {
        //DirectedGraph<State> graph = new DirectedGraph<>();
        
        ArgumentParser argPars = ArgumentParser.of(args, "Checker");
        BasicGraph basicGraphClass;
        DBinterface dbInterface;
        if(argPars.isDutch()){
            dbInterface = new DBinterface("SQLite/token_database_dutch.db", "SQLite/DutchTranslation.txt");
            basicGraphClass = new BasicGraph();
        }else if(argPars.isTurkish()){
            dbInterface = new DBinterface("SQLite/token_database_turkish.db", "SQLite/TurkishTranslation.txt");
            basicGraphClass = new BasicGraph(true);
        }else{
            dbInterface = new DBinterface("SQLite/token_database_english.db", "SQLite/smallDic.txt");
            basicGraphClass = new BasicGraph();
        }   
        DirectedGraph<State> graph = basicGraphClass.getGraph();
        JsonMaker jsonMaker = JsonMaker.create();

        if(argPars.isValidateUpdates()){
            
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {

                    String dbName = "SQLite/token_database_english_updated.db";
                    if(argPars.isCheckFile()){
                        dbName = argPars.getFileName();
                    }
                    new WordRoleUpdater(dbName);
                }
            });
        }else if(argPars.isUpdateTokenFromDic()){
            dbInterface.readDataFromDatabase();
            if(argPars.isDutch()){
                dbInterface.updateTokenTableFromDic("SQLite/token_database_dutch.db", "SQLite/DutchTranslation.txt", true);
            }else if(argPars.isTurkish()){
                dbInterface.updateTokenTableFromDic("SQLite/token_database_turkish.db", "SQLite/TurkishTranslation.txt", true);
            }else{
                dbInterface.updateTokenTableFromDic("SQLite/token_database_english.db", "SQLite/DutchTranslation.txt", false);
            }
            
        }else if(argPars.isUpdateToken()){
            dbInterface.readDataFromDatabase();
            if(argPars.isCheckFile()){
                SentenceExtractor extractor = SentenceExtractor.of(argPars.getFileName());
                List<String> extractedSentences = extractor.getSentences();  
                int i = 0;
                int n = extractedSentences.size();
                int cntUpdate = 0;
                for (String sentence : extractedSentences) {
                    i++;
                    //dbInterface.updateTokenInDatabase(sentence.toLowerCase(), graph);
                    sentence = StringProcessor.handleApostrophe(sentence.toLowerCase(), argPars.isTurkish());
                    PhraseExtractor extractorPhrase = PhraseExtractor.fromSentence(sentence, 3, 5);
                    List<String> phrases = extractorPhrase.getPhrases();
                    for (String phrase : phrases) {
                        cntUpdate += dbInterface.updateTokenInDatabase(phrase.toLowerCase(), graph);
                    } 
                    ProgressBar.printProgress(((double)i)/((double)n));                     
                }
                System.out.println("\n-------------------------------------------\n"+ "Number of update: "+ cntUpdate);
            }else if(argPars.isCheckSentence()){
                //dbInterface.updateTokenInDatabase(argPars.getSentence().toLowerCase(), graph);
                for (String phrase : PhraseExtractor.fromSentence(StringProcessor.handleApostrophe(argPars.getSentence(), argPars.isTurkish()),3, 5).getPhrases()) {
                    dbInterface.updateTokenInDatabase(phrase.toLowerCase(), graph);                    
                }
            }
            if(argPars.isDutch()){
                dbInterface.updateDatabase("SQLite/token_database_dutch_updated.db");
            }else if(argPars.isTurkish()){
                dbInterface.updateDatabase("SQLite/token_database_turkish_updated.db");
            }else{
                dbInterface.updateDatabase("SQLite/token_database_english_updated.db");
            }
        }else if(argPars.isUpdateHashTable()){
            if(argPars.isCheckFile()){
                SentenceExtractor extractor = SentenceExtractor.of(argPars.getFileName());
                List<String> extractedSentences = extractor.getSentences();  
                
                try {
                    HashTableMaker manager;
                    if(argPars.isDutch()){
                        manager = new HashTableMaker("SQLite/hash_database_dutch.db");
                    }else if(argPars.isTurkish()){
                        manager = new HashTableMaker("SQLite/hash_database_turkish.db");
                    }else{
                        manager = new HashTableMaker("SQLite/hash_database_english.db");
                    }
                    for (String sentence : extractedSentences) {
                        sentence = StringProcessor.handleApostrophe(sentence.toLowerCase(), argPars.isTurkish());
                        manager.updateDatabase(sentence.toLowerCase());
                        PhraseExtractor extractorPhrase = PhraseExtractor.fromSentence(sentence, 1, 4);
                        List<String> phrases = extractorPhrase.getPhrases();
                        for (String phrase : phrases) {
                            manager.updateDatabase(phrase.toLowerCase());
                        }                        
                    }
                    manager.closeConnection();
                } catch (SQLException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }else if(argPars.isCheckSentence()){
                try {
                    HashTableMaker manager = new HashTableMaker("SQLite/hash_database_english.db");
                    manager.updateDatabase(StringProcessor.handleApostrophe(argPars.getSentence().toLowerCase(), argPars.isTurkish()));
                    for (String phrase : PhraseExtractor.fromSentence(StringProcessor.handleApostrophe(argPars.getSentence().toLowerCase(), argPars.isTurkish()),1, 4).getPhrases()) {
                        manager.updateDatabase(phrase.toLowerCase());                      
                    }
                    manager.closeConnection();
                } catch (SQLException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        }else if(argPars.isCheckFile()){
            SentenceExtractor extractor = SentenceExtractor.of(argPars.getFileName());
            List<String> extractedSentences = extractor.getSentences();  
            try {
                HashTableMaker manager = new HashTableMaker("SQLite/hash_database_english.db");
                for (String sentence : extractedSentences) {
                    System.out.println("Sentence: " + sentence);
                    sentence = StringProcessor.handleApostrophe(sentence.toLowerCase(), argPars.isTurkish());
                    System.out.println("*********************************************************");
                    PhraseExtractor extractorPhrase = PhraseExtractor.fromSentence(sentence);
                    List<String> phrases = extractorPhrase.getPhrases();
                    int ngram        = manager.nGram(sentence, 3);
                    int stateMachine = dbInterface.checkTokenInDatabase(sentence.toLowerCase(), graph);
                    int conf         = (ngram>=0)?(int)(ngram*0.2+stateMachine*0.8):stateMachine;
                    jsonMaker.addSentence(sentence.toLowerCase(), conf);
                    for (String phrase : phrases) {
                        System.out.println("Phrase: "+ phrase);
                        ngram        = manager.nGram(phrase.toLowerCase(), 3);
                        stateMachine = dbInterface.checkTokenInDatabase(phrase.toLowerCase(), graph);
                        conf         = (ngram>=0)?(int)(ngram*0.2+stateMachine*0.8):stateMachine;
                        jsonMaker.addPhrase(phrase.toLowerCase(), conf);
                        System.out.println("------------------------------------------------------------");
                        
                    }
                    
                    jsonMaker.toJson("confidence_ourChecker.json");
                    System.out.println("##########################################################");
                    
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if(argPars.isCheckSentence()){
            try {
                HashTableMaker manager = new HashTableMaker("SQLite/hash_database_english.db");
                System.out.println("Sentence: " + argPars.getSentence());
                String sentence = StringProcessor.handleApostrophe(argPars.getSentence().toLowerCase(), argPars.isTurkish());
                int ngram        = manager.nGram(sentence, 3);
                int stateMachine = dbInterface.checkTokenInDatabase(sentence.toLowerCase(), graph);
                int conf         = (ngram>=0)?(int)(ngram*0.2+stateMachine*0.8):stateMachine;
                jsonMaker.addSentence(sentence, conf);
                System.out.println("*********************************************************");
                PhraseExtractor extractorPhrase = PhraseExtractor.fromSentence(StringProcessor.handleApostrophe(argPars.getSentence().toLowerCase(), argPars.isTurkish()));
                List<String> phrases = extractorPhrase.getPhrases();
                for (String phrase : phrases) {
                    ngram        = manager.nGram(phrase.toLowerCase(), 3);
                    stateMachine = dbInterface.checkTokenInDatabase(phrase.toLowerCase(), graph);
                    conf         = (ngram>=0)?(int)(ngram*0.2+stateMachine*0.8):stateMachine;
                    System.out.println("Phrase: " + phrase);
                    jsonMaker.addPhrase(phrase, conf);
                    System.out.println("------------------------------------------------------------");
                }
                jsonMaker.toJson("confidence_ourChecker.json");
                System.out.println("##########################################################");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if(argPars.isCheckGUI()){
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new HighlighterGUI(argPars.isDutch(), argPars.isTurkish());
                }
            });
        }
          
    }
}

//javac -d bin Checker.java **/*.java
//java -cp bin:SQLite/sqlite-jdbc-3.45.2.0.jar:SQLite/slf4j-api-1.7.36.jar:SQLite/slf4j-jdk14-1.7.36.jar Checker 
//jar cvfm checker.jar manifest.txt -C bin . -C SQLite .
