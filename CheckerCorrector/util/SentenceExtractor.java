package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SentenceExtractor {
    private List<String> sentences;

    private SentenceExtractor(List<String> sentences) {
        this.sentences = sentences;
    }

    public static SentenceExtractor of(String filePath) {
        //System.out.println(filePath);
        List<String> sentences = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                
                String[] parts = line.split("\\.");
                for (String part : parts) {
                    
                    sb.append(part).append(".");
                    
                    if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '.') {
                        sentences.add(sb.toString().trim());
                        sb.setLength(0);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new SentenceExtractor(sentences);
    }

    public static SentenceExtractor ofLine(String line) {
        //System.out.println(filePath);
        List<String> sentences = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        String[] parts = line.split("\\.");
        for (String part : parts) {
                    
            sb.append(part).append(".");
                    
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '.') {
                sentences.add(sb.toString().trim());
                sb.setLength(0);
            }
        }
        return new SentenceExtractor(sentences);
    }

    public List<String> getSentences() {
        return sentences;
    }
}
