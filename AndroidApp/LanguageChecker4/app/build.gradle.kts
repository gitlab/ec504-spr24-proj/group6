plugins {
    alias(libs.plugins.androidApplication)
}

android {
    namespace = "com.example.languagechecker"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.languagechecker"
        minSdk = 29
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    dependenciesInfo {
        includeInBundle = false
    }
}

dependencies {

    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.activity)
    implementation(libs.constraintlayout)
    implementation(files("libs\\slf4j-api-1.7.36.jar"))
    implementation(files("libs\\slf4j-jdk14-1.7.36.jar"))
    implementation(files("libs\\sqlite-jdbc-3.45.2.0.jar"))
    implementation(files("libs\\checker.jar"))
    testImplementation(libs.junit)
    androidTestImplementation(libs.ext.junit)
    androidTestImplementation(libs.espresso.core)
}