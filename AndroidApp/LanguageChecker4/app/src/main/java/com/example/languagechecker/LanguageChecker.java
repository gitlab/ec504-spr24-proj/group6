package com.example.languagechecker;

import com.example.CheckerCorrector.Checker;

public class LanguageChecker {
    private Checker internal_checker;

    public LanguageChecker() {
        internal_checker = new Checker();
    }

    public String analyzeSentence(String sentence, String db_path, String db_hash_path, String dic_path){
        return internal_checker.checkerAndroidInterface(sentence,db_path, db_hash_path,dic_path);
        //return "5";
    }
}
